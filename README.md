Description:

Simulates a distributed hash table implementing the Chord algorithm for node management.

Prerequisites:

Sbt and Scala

Instructions to run the program:

* sbt build at the project root.

* sbt “run <parameters>” at the project root will execute the program.

Parameters can be

1. autotest - 
will run a suite of tests cycling through the algorithms and number of nodes.

2. <numNodes> <numRequests> - 
Will run that exact scenario. numNodes is an integer value with number of nodes, numRequests is an integer value with number of requests per node to be processed.