package Chord

import java.security.MessageDigest
import java.math.BigInteger
import java.lang.Math.log


object SHA1 {
	var valM: Int = 160

	def initialize (num: Double) = {

		val size = (log(num)/log(2)).toInt + 1
		if(size < (160-20))
			valM = (size + 20)
		else
			valM = 160
		//println(valM + " " + size + " " + num)
	}

	def hashBI (seed: String) : BigInteger = {
		val md = MessageDigest.getInstance("SHA-1")
		md.update(seed.getBytes("UTF-8"))
		val hashedval: BigInteger = new BigInteger(1, md.digest())
		val cap: BigInteger =  BigInteger.valueOf(0).setBit(valM)
		return hashedval.remainder(cap)
	}

	def hashString (seed: String) : String = {
		val md = MessageDigest.getInstance("SHA-1")
		md.update(seed.getBytes("UTF-8"))
		return md.digest().map("%02x".format(_)).mkString
	}
}
