package Chord

import akka.actor._
import java.math.BigInteger
import java.util.concurrent.TimeUnit
import java.util.concurrent.ScheduledFuture
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.collection.mutable.Map


case class initialize(name: String, actor: ActorRef, system: ActorSystem, tti: Int)
case class findSuccessor(id: BigInteger, queror: ActorRef, hops: Int)
case class successorResult(id: BigInteger, pair: (ActorRef, BigInteger), hops: Int)
case class notifyPredecessor(pair: (ActorRef, BigInteger))
case class notifySuccessors(successors: Array[(ActorRef, BigInteger)])
case class storeData(data: (String, String))
case class getData(key: String)
case class storeKeyed(data: (BigInteger, String))
case class getKeyed(queror: ActorRef, key: BigInteger, hops: Int)

class Node extends Actor{
	val debug: Boolean = false


	var actorName: String = ""
	var value: BigInteger = BigInteger.valueOf(0)

	var neighbors: Array[(ActorRef, BigInteger)] = new Array[(ActorRef, BigInteger)](6)
	var fingers: Array[(ActorRef, BigInteger)] = new Array[(ActorRef, BigInteger)](SHA1.valM)
	var fingerRefs: Array[BigInteger] = new Array[BigInteger](SHA1.valM)
	var initFingers: Boolean = false
	var neighborsInitialized: Boolean = false
	var storedValues: Map[BigInteger, String] = Map[BigInteger, String]()

	var scheduledTask: Cancellable = null

	def checkBelongs(id1: BigInteger, start: BigInteger, end1: BigInteger): Boolean = {
		if(debug) println("checkBelongs")
		var id: BigInteger = BigInteger.valueOf(0)
		var end: BigInteger = BigInteger.valueOf(0)
		if(id1.compareTo(start) < 0) id = id1.setBit(SHA1.valM)
			else id = id1
		if(end1.compareTo(start) <= 0) end = end1.setBit(SHA1.valM)
			else end = end1
		if((id.compareTo(start) > 0) && (id.compareTo(end) < 0)) return true
			else return false
	}

	def closestPrecedingNode(id: BigInteger): ActorRef = {
		if(debug) println("closestPrecedingNode")
		var fingersToBeUpdated: Boolean = false
		for(i <- (1 until SHA1.valM).reverse){
			if(checkBelongs(fingers(i)._2, value, id) || fingers(i)._1 == neighbors(1)._1){
				if(fingers(i)._1.isTerminated == false)
					return fingers(i)._1
				else
					fingersToBeUpdated = true
			}
		}
		if(fingersToBeUpdated)
			context.self ! "maintenance"
		return neighbors(1)._1
	}

	def fixFingers() = {
		if(debug) println("fixFingers" + context.self)
		for(i <- 0 until SHA1.valM)
			context.self ! findSuccessor(fingerRefs(i), context.self, 0)
	}

	def checkNeighbors() = {
		if(debug) println("checkNeighbors")
		if(neighborsInitialized)
			if(neighbors(1)._1.isTerminated == true)
				for(id <- (neighbors.length-1) to 2)
					if(neighbors(id)._1.isTerminated == false){
						neighbors(1) = neighbors(id)
						neighbors(1)._1 ! notifyPredecessor(context.self -> value)
					}
	}


	override def postStop() {
// 		scheduledTask.cancel()
	}

	def receive = {

		case initialize(name: String, actor: ActorRef, system: ActorSystem, tti: Int) => {
			actorName = name
			value = SHA1.hashBI(name)
			//println(value)
			for(i <- 0 until SHA1.valM)
				fingerRefs(i) = value.add(BigInteger.valueOf(0).setBit(i)).clearBit(SHA1.valM)

			if(actor == context.self){
				for(id <- 0 to 5)
					neighbors(id) = context.self -> value
				for(id <- 0 until SHA1.valM)
					fingers(id) = context.self -> value
				neighborsInitialized = true
			}
			else{
				actor ! findSuccessor(value, context.self, 0)
			}

			scheduledTask = system.scheduler.schedule(Duration(tti, SECONDS), Duration(tti/2, SECONDS), context.self, "maintenance")
		}

		case findSuccessor(id: BigInteger, queror: ActorRef, hops: Int) => {
			if(debug) println("findSuccessor")
			if(neighborsInitialized){
				val newHops: Int = hops + 1
				if(checkBelongs(id, value, neighbors(1)._2) || id == neighbors(1)._2)
					queror ! successorResult(id, neighbors(1), newHops)
				else
					closestPrecedingNode(id) ! findSuccessor(id, queror, newHops)
			}
			else
				self ! findSuccessor(id, queror, hops)
		}

		case successorResult(id: BigInteger, pair: (ActorRef, BigInteger), hops: Int) => {
			if(debug) println("successorResult")
			if(id == value)
				pair._1 ! notifyPredecessor(context.self -> value)
			else
				for(i <- 0 until SHA1.valM)
					if(fingerRefs(i) == id){
						fingers(i) = pair
					}
		}

		//pair thinks its the predecessor
		case notifyPredecessor(pair: (ActorRef, BigInteger)) => {
			if(debug) println("notify predecessors for " + context.self + " from " + pair._1)

			if(!neighborsInitialized){
				neighbors(0) = pair
				}
			else if(neighbors(0)._1.isTerminated == true){
				neighbors(0) = pair
				val neighToBePassed = neighbors
				neighToBePassed(0) = (context.self -> value)
				neighbors(0)._1 ! notifySuccessors(neighToBePassed)
				}
			else if(neighbors(0)._1 == pair._1){}
			else if (checkBelongs(pair._2, neighbors(0)._2, value)){
				val temp: (ActorRef, BigInteger) = neighbors(0)
				neighbors(0) = pair
				val neighToBePassed = neighbors.clone()
				neighToBePassed(0) = (context.self -> value)
				neighbors(0)._1 ! notifyPredecessor(temp)
				neighbors(0)._1 ! notifySuccessors(neighToBePassed)
			}
			else if(checkBelongs(neighbors(0)._2, pair._2, value))
				neighbors(0)._1 ! notifyPredecessor(pair)
		}

		//successor is updating the node's successor table and propogate the changes, if any
		case notifySuccessors(successors: Array[(ActorRef, BigInteger)])=> {
			if(debug) println("notify successors for " + context.self + " First Entry is " + successors(0)._1)
			var propogate: Boolean = false
			if(!neighborsInitialized){
				for(id <- 1 until neighbors.length)
					neighbors(id) = successors(id-1)
				neighborsInitialized = true
				propogate = true
			}
			else{
				for(id <- 1 until neighbors.length)
					if(neighbors(id)._1 != successors(id-1)._1){
						neighbors(id) = successors(id-1)
						if(id < (neighbors.length -1)) propogate = true
					}
			}
			if(propogate){
				val neighToBePassed = neighbors.clone()
				neighToBePassed(0) = (context.self -> value)
				neighbors(0)._1 ! notifySuccessors(neighToBePassed)
			}
			if(!initFingers){
				for(i <- 0 until SHA1.valM)
					fingers(i) = neighbors(1)
				initFingers = true
			}
			fixFingers()
		}

		case "maintenance" => {
			fixFingers()
			checkNeighbors()
		}

		case storeData(data: (String, String)) => {
			context.self ! storeKeyed(SHA1.hashBI(data._1) -> data._2)
		}

		case getData(key: String) => {
			context.self ! getKeyed(sender, SHA1.hashBI(key), 0)
		}

		case storeKeyed(data: (BigInteger, String)) => {
			if(debug) println("storeKeyed")

			if(checkBelongs(data._1, neighbors(0)._2, value) || data._1 == value)
				storedValues(data._1) = data._2
			else if(checkBelongs(data._1, value, neighbors(1)._2) || data._1 == neighbors(1)._2)
				neighbors(1)._1 ! storeKeyed(data)
			else
				closestPrecedingNode(data._1) ! storeKeyed(data)
		}

		case getKeyed(queror: ActorRef, key: BigInteger, hops: Int) => {
			if(debug) println("getKeyed")

			if(checkBelongs(key, neighbors(0)._2, value) || key == value){
				if(storedValues.contains(key))
					queror ! getDataResult((key -> storedValues(key)), hops)
			}
			else if(checkBelongs(key, value, neighbors(1)._2) || key == neighbors(1)._2)
				neighbors(1)._1 ! getKeyed(queror, key, hops + 1)
			else
				closestPrecedingNode(key) ! getKeyed(queror, key, hops + 1)
		}

		case "debugFingerTable" => {
			for(it <- 0 until 6)
				println(value + " " + neighbors(it))
			for(it <- 0 until SHA1.valM)
				println(value + " " + fingerRefs(it) + " " + fingers(it))
		}
	}
}
