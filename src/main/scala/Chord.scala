package Chord

import akka.actor._
import scala.concurrent.Await
import scala.concurrent.Future
import akka.util.Timeout
import scala.concurrent.duration._
import akka.pattern.ask
import com.typesafe.config._

object ChordSim {

	def test(numNodes: Int, numRequests: Int, kill: Boolean = false): Boolean = {

		val configString: String = """akka {
			  log-dead-letters-during-shutdown = off
		}"""
// 			  log-dead-letters = off

			val system = ActorSystem("Chord", ConfigFactory.parseString(configString))
			val admin = system.actorOf(Props[Admin], "Admin")


			implicit val timeout = Timeout(200 seconds)
			var future = admin ? initlocal(numNodes, numRequests, system)
			val result = Await.result(future, timeout.duration).asInstanceOf[String]

// 			println("shutting system down")
			system.shutdown()

			if(result == "success")
				return true
			else
				return false
	}


	def main(args: Array[String]) {
		if(args(0) != "autotest"){
			var numNodes: Int = args(0).toInt
			var numRequests: Int = args(1).toInt

			test(numNodes, numRequests)
		}
		else{
			for(i <- (1 to 500))//.reverse)
				test(i * 100, 10)
		}
	}
}

// object Application extends App {
// }
