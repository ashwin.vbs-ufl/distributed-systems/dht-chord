package Chord

import akka.actor._
import scala.util.Random
import java.math.BigInteger
import scala.collection.mutable.Map
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global
import java.lang.Math.log

case class initlocal(numNodes: Int, numRequests: Int, system: ActorSystem)
case class getDataResult(data: (BigInteger, String), hops: Int)

class Admin extends Actor{
	var start: Int = 0
	var storedValues: Map[String, String] = Map[String, String]()
	var numResults: Int = 0
	var totalHops: Int = 0
	var numNodes: Int = 0
	var numRequests: Int = 0
	var refNode: ActorRef = null
	var source: ActorRef = null
	var system: ActorSystem = null
	var timeToInit: Int = 0

	def receive = {
		case initlocal(numNodes1: Int, numRequests1: Int, system1: ActorSystem) => {
			timeToInit = numNodes1 / 1000
			if(timeToInit < 8) timeToInit = 8
			SHA1.initialize(numNodes1.toDouble * numRequests1.toDouble)
			source = sender
			system = system1
			numNodes = numNodes1
			numRequests = numRequests1
			start = Random.nextInt(100)
			refNode = context.actorOf(Props[Node], "Node" + start.toString)
			refNode ! initialize("Node" + start.toString, refNode, system, timeToInit/2)
			for(it <- 1 until numNodes){
				var temp = context.actorOf(Props[Node], "Node" + (it+start).toString)
				temp ! initialize("Node" + (it+start).toString, refNode, system, timeToInit/2)
			}

			system.scheduler.scheduleOnce(Duration(timeToInit, SECONDS), context.self, "kickOff")
		}

		case "kickOff" => {
// 			refNode ! "debugFingerTable"
			for(it <- 0 until (numNodes*numRequests))
				storedValues(SHA1.hashString((2*it).toString())) = SHA1.hashString(((2*it) + 1).toString())

			for(it <- storedValues.keySet)
				refNode ! storeData(it -> storedValues(it))

			system.scheduler.scheduleOnce(Duration(timeToInit/2, SECONDS), context.self, "startQueries")
		}

		case "startQueries" => {
			var nodeID: Int = start
			for(it <- storedValues.keySet){
				context.actorFor("Node" + nodeID) ! getData(it)
				nodeID = nodeID + 1
				if(nodeID >= (start + numNodes))
					nodeID = start
			}
		}

		case getDataResult(data: (BigInteger, String), hops: Int) => {
			totalHops = totalHops + hops
			numResults = numResults + 1
			if(numResults == storedValues.size-1){
				println("Simulation with " + numNodes + " Nodes resulted in an average of " + (totalHops/numResults) + " hops per data request")
				source ! "success"
			}
		}
	}
}
